import { Route } from "react-router-dom";

/**
* This will return the route with its props
*/
export const PublicRoute = props => {
    return <Route {...props} />
}

export default PublicRoute
