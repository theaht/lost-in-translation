

/**
 * This will set the local storage,
 * it encrypts a stringified version of the storage value and
 * set the localstorage with the key and the encrypted version.
 * */
export const setStorage = (key, value) => {
    try {
        const encrypt = btoa(JSON.stringify(value))
        localStorage.setItem(key, encrypt)
    } catch (e) {
        console.log(e)
    }

}

/**
 * This uses the key to get the storage item, decrypts the value of the storage and
 * parses it into a JSON object. It returns the JSON object.
 * If the storage is empty, null is returned.
 * */
export const getStorage = ( key ) => {

    const storage = localStorage.getItem( key )
    if(!storage){
        return null
    }
    try {
        const decrypt = atob(storage)
        return JSON.parse(decrypt)
    } catch (e){
        return null
    }

}