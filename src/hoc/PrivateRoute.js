import { Route, Redirect } from "react-router-dom";
import { getStorage } from "../utils/localStorage"
import { AppRoutes } from "../consts/AppRoutes";


/**
 * If local-storage is empty, and the user tries to access a private route,
 * the user will be redirected to the login-page.
 * Otherwise it will return the route with its props.
 * */
export const PrivateRoute = props => {

    const session = getStorage('ra_session')

    if (session === null) {
        return <Redirect to={ AppRoutes.Login }  />
    }

    return <Route {...props} />
}

export default PrivateRoute
